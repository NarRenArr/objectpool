﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    private void OnBecameInvisible()
    {
        GetComponent<PoolObject>().ReturnToPool();
    }
}
