﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public int maxEnemy = 3;
    [SerializeField] private Enemy _enemy;

    private void Update()
    {
        SpawnEnemy();
    }

    private void SpawnEnemy()
    {
        if (FindObjectsOfType<Enemy>().Length < maxEnemy)
        {
            Vector3 spawningPos = new Vector3(Random.Range(-4f, 4f), 5, 0);

            GameObject enemy = PoolManager.GetObject("Enemy", spawningPos, new Quaternion(0, 0, 0, 0));
        }
    }
}
