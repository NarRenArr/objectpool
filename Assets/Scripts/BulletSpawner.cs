﻿using UnityEngine;

public class BulletSpawner : MonoBehaviour
{
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 spawningPos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10));

            GameObject bullet = PoolManager.GetObject("Bullet", spawningPos, new Quaternion(0, 0, 0, 0));
        }
    }
}
