﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int force;

    private float _lifeTime = 3;

    private void Update()
    {
        if (_lifeTime < 0)
            GetComponent<PoolObject>().ReturnToPool();

        _lifeTime -= Time.deltaTime;
    }

    private void OnEnable()
    {
        GetComponent<Rigidbody>().velocity = Vector3.up * force;
        _lifeTime = 3;
    }

    private void OnBecameInvisible()
    {
        GetComponent<PoolObject>().ReturnToPool();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            collision.gameObject.GetComponent<PoolObject>().ReturnToPool();
            GetComponent<PoolObject>().ReturnToPool();
        }
    }
}
